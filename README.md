Hi,
#### Sample App in Python that implements Connect to Quickbooks button and imports customer and inventory data from QBO to Django Rest Framework.

- Implementing OAuth2.0 to connect an application to a customer's QuickBooks Online company .
- Using python-quickbooks library to get connect to the Details in Quickbook and fetching it
- Displaying details in Rest API Format.

## Requirements
1. Python 3.6
2. A [developer.intuit.com](https://developer.intuit.com/) account
3. An app on [developer.intuit.com](https://developer.intuit.com/) and the associated app keys:  
    - Client Id and Client Secret for OAuth2 apps; Configure the RedirectUri[http://localhost:5000/callback] in your app's Keys tab on the Intuit developer account, only Accounting scope needed  
4. This sample app uses several libraries listed in [requirements.txt](requirements.txt) which need to be installed.

## Manually connecting with OAuth version 2.0

1.Create the Authorization URL for your application:

       from quickbooks import Oauth2SessionManager
       
       callback_url = 'http://localhost:8000' # Quickbooks will send the response to this url

       session_manager = Oauth2SessionManager(
           client_id=QUICKBOOKS_CLIENT_ID,
           client_secret=QUICKBOOKS_CLIENT_SECRET,
           base_url=callback_url,
       )

       authorize_url = session_manager.get_authorize_url(callback_url)

2.Redirect to the authorize_url. Quickbooks will redirect back to your callback_url.

3.Handle the callback:

		session_manager = Oauth2SessionManager(
		    client_id=QUICKBOOKS_CLIENT_ID,
		    client_secret=QUICKBOOKS_CLIENT_SECRET,
		    base_url=callback_url, # the base_url has to be the same as the one used in authorization
		)

		# caution! invalid requests return {"error":"invalid_grant"} quietly
		session_manager.get_access_tokens(request.GET['code'])
		access_token = session_manager.access_token
		refresh_token = session_manager.refresh_token

Store access_token and refresh_token for later use. 

## Accessing the API
OAuth version 2.0 - Setup the session manager using the stored ``access_token`` and ``realm_id``:


        session_manager = Oauth2SessionManager(
            client_id=realm_id,
            client_secret=CLIENT_SECRET,
            access_token=AUTH2_ACCESS_TOKEN,
        )

Then create the QuickBooks client object passing in the session manager:
   
  	from quickbooks import QuickBooks
    client = QuickBooks(
        sandbox=True,
        session_manager=session_manager,
        company_id=realm_id
    )

You can disconnect the current Quickbooks Account like so (See `Disconnect documentation`_ for full details):

    client.disconnect_account()

If your consumer_key never changes you can enable the client to stay running:

    QuickBooks.enable_global()

You can disable the global client like so:

    QuickBooks.disable_global()


List of objects:

    from quickbooks.objects.customer import Customer
    customers = Customer.all(qb=client)

**Note:** The maximum number of entities that can be returned in a response is 1000. If the result size is not specified, the default
number is 100. (See `Intuit developer guide`_ for details)

#### Quickbooks Sample App Example: https://help.developer.intuit.com/s/samplefeedback?cid=9010&repoName=SampleApp-QuickBooksV3API-Python
#### quickbooks-python: https://github.com/sidecars/python-quickbooks
#### Intuit developer guide: https://developer.intuit.com/docs/0100_accounting/0300_developer_guides/querying_data