from quickbooks import QuickBooks, Oauth2SessionManager, exceptions
import os

callback_url = os.getenv("CALLBACK_URL")
session_manager = Oauth2SessionManager(
    client_id=os.getenv("CLIENT_ID"),
    client_secret=os.getenv("CLIENT_SECRET"),
    base_url=callback_url,
)
realmid = os.getenv("REALM_ID")

def refresh_token():
    """Function to refresh access token and save in DB"""
    session_manager.refresh_access_tokens(os.getenv("REFRESH_TOKEN"))
    os.environ['ACCESS_TOKEN'] = session_manager.access_token
    os.environ['REFRESH_TOKEN'] = session_manager.refresh_token

def generate_client():
    sessionmanager = Oauth2SessionManager(
        client_id=realmid,
        client_secret=os.getenv("CLIENT_SECRET"),
        access_token=os.getenv("ACCESS_TOKEN"),
        )
    client = QuickBooks(
        sandbox=True,
        session_manager=sessionmanager,
        company_id=realmid,
        )
    return client