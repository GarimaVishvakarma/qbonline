from django.apps import AppConfig


class QuickbookConfig(AppConfig):
    name = 'qbapp'
