from django.db import models

# Create your models here.
class TokenDetails(models.Model):
    access_token = models.TextField(blank=False)
    refresh_token = models.CharField(max_length=255, blank=False)
    realm_id = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return ("Token Details of Company Id %s") % self.realm_id