from django.conf.urls import include, url
from .views import CustomerView, InventoryView,  quickbook_details


urlpatterns = [
	url(r'^$', quickbook_details, name='details'),
    url(r'^customers/$', CustomerView.as_view(), name='quickbook-customer-view'),
    url(r'^inventory/$', InventoryView.as_view(),name='quickbook-inventory-view'),
]
