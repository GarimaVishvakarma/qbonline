from django.shortcuts import redirect, render
from quickbooks.objects.customer import Customer
from quickbooks.objects.item import Item
from .constants import refresh_token, generate_client
from rest_framework import views
from rest_framework.response import Response
from .serializers import CustomerSerializer, ItemSerializer
from quickbooks import exceptions
from rest_framework.status import HTTP_200_OK
import logging
logger = logging.getLogger(__name__)


def quickbook_details(request):
    """ Show page to display Customers and Inventory Details Option after Callback"""
    return render(request, "callback.html")


class CustomerView(views.APIView):
    """API View to show Customer's Details"""

    def get(self, request):
        try:
            client = generate_client()
            item = Customer.all(qb=client)
            results = CustomerSerializer(item, many=True).data
        except exceptions.QuickbooksException as e:
            refresh_token()
            logger.info("Generating access token again with refresh token. Old Token Expired")
            return redirect('quickbook-customer-view')
        except ConnectionError:
            logger.warning("Connection Error")
            return redirect("quickbook-customer-view")
        return Response(results,status=HTTP_200_OK)


class InventoryView(views.APIView):
    """API View to show Inventory's Details"""

    def get(self, request):
        try:
            client = generate_client()
            item = Item.all(qb=client)
            results = ItemSerializer(item, many=True).data
        except exceptions.QuickbooksException as e:
            refresh_token()
            logger.info("Generating access token again with refresh token. Old Token Expired")
            return redirect('quickbook-inventory-view')
        except ConnectionError:
            logger.warning("Connection Error")
            return redirect("quickbook-customer-view")
        return Response(results,status=HTTP_200_OK)