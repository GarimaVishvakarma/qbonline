from rest_framework import serializers


class CustomerSerializer(serializers.Serializer):
    """Your data serializer for customer Details"""
    given_name = serializers.CharField(source="GivenName")
    middle_name = serializers.CharField(source="MiddleName")
    family_name = serializers.CharField(source="FamilyName")
    company_name = serializers.CharField(source="CompanyName")
    primary_phone = serializers.CharField(source="PrimaryPhone")
    balance = serializers.FloatField(source="Balance")


class ItemSerializer(serializers.Serializer):
    """Your data serializerfor Inventory Details"""
    name = serializers.CharField(source="Name")
    type_of_item = serializers.CharField(source="Type")
    description = serializers.CharField(source="Description")
    unit_price = serializers.FloatField(source="UnitPrice")
    taxable = serializers.BooleanField(source="Taxable")
    qty_on_hand = serializers.FloatField(source="QtyOnHand")
